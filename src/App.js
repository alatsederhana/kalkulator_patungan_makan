import './App.css';
import { useState } from 'react';
import { FiUser } from "react-icons/fi";

export default function App() {

  const [totlaBayar, setTotalbayar] = useState()
  const [potonganHarga, setPotonganHarga] = useState()
  const [ongkir, setOngkir] = useState(0)
  const [jumlahOrang, setJumlahOrang] = useState()
  const [patunganBedaHarga, setPatunganBedaHarga] = useState(false)
  const [nominalPatunganPerOrang, setNominalPatunganperOrang] = useState([])
  const [statusHasilHitung, setStatusHasilHitung] = useState(false)

  //variable hasil
  const [hasilNominalPatunganPerOrang, setHasilNominalPatunganperOrang] = useState([])
  const [hasilOngkir, setHasilOngkir] = useState([])


  //function hitung
  const hitung = () => {

    // harga item perorang tidak lebih besar dari total pesanan


    const persentase = (potonganHarga / totlaBayar)
    let tmpHasilHitungPerOrang = []
    let tmpOngkir = []

    if(patunganBedaHarga){

      const sum = nominalPatunganPerOrang.reduce((partialSum, a) => partialSum + parseInt(a), 0);
      console.log(sum)
      if(sum !== parseInt(totlaBayar)){
        alert("Total harga per orang tidak sesuai dengan total")
        return
      }

      
      for (let i = 0; i < nominalPatunganPerOrang.length; i++) {
        const nominal = nominalPatunganPerOrang[i];
        tmpHasilHitungPerOrang.push(nominal - (nominal*persentase))
        tmpOngkir.push(ongkir/jumlahOrang)
      }

    }else{

      const total = totlaBayar - potonganHarga;
      for (let i = 0; i  < jumlahOrang; i++) {
        tmpHasilHitungPerOrang.push(total/jumlahOrang)
        tmpOngkir.push(ongkir/jumlahOrang)
      }

    }

    setHasilNominalPatunganperOrang(tmpHasilHitungPerOrang)
    setHasilOngkir(tmpOngkir)
    setStatusHasilHitung(true)
    

  }

  const reset = () => {
    setStatusHasilHitung(false)
  }

  const formatNumber = (n) => {
    var parts = n.toString().split(".");
    const numberPart = parts[0];
    const decimalPart = parts[1];
    const thousands = /\B(?=(\d{3})+(?!\d))/g;
    return numberPart.replace(thousands, ".") + (decimalPart ? "," + decimalPart : "");
  }

  const HasilHitung = () => {

    return <div class="w-full">
    <form class="px-8 pt-6 pb-8 mb-4 mt-0">
      <div class="mb-4 flex flex-row justify-between">
        <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
          Total Pesanan
        </label>
        <label class="block text-gray-700 text-md font-bold mb-2" for="total_bayar">
          Rp{formatNumber(totlaBayar)}
        </label>
      </div>

      <div class="mb-4 flex flex-row justify-between">
        <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
          Total Ongkir
        </label>
        <label class="block text-gray-700 text-md font-bold mb-2" for="total_bayar">
        Rp{formatNumber(ongkir)}
        </label>
      </div>

      <div class="mb-4 flex flex-row justify-between">
        <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
          Total discount / potongan harga
        </label>
        <label class="block text-gray-700 text-md font-bold mb-2" for="total_bayar">
        Rp{formatNumber(potonganHarga)}
        </label>
      </div>

      <div class="mb-4 flex flex-row justify-between">
        <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
          Jumlah Orang
        </label>
        <label class="block text-gray-700 text-md font-bold mb-2" for="total_bayar">
          {jumlahOrang} Orang
        </label>
      </div>

      <hr></hr>

      <div className='mb-4 mt-4 justify-start'>
        <div class="mb-6">
          <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
            Nominal yang harus di bayar tiap orang
          </label>
        </div>
      </div>

      <div class="flex flex-row justify-between">
        <div className='flex flex-row justify-start'>
          
        </div>
        <div className='flex flex-row w-2/3'>
          <div className='flex justify-end w-1/2'>
            <label class="block text-gray-400 text-md font-bold mb-2">
              Detail
            </label>
          </div>
          <div className='flex justify-end w-1/2'>
            <label class="block text-gray-400 text-md font-bold mb-2">
              Total
            </label>
          </div>
        </div>
      </div>

      <hr></hr>

      <br></br>
      
      {hasilNominalPatunganPerOrang.map((val,idx) => <div class="mb-4 flex flex-row justify-between">
        <div className='flex flex-row justify-start'>
          <label class="text-gray-500 text-md font-bold mb-2 mr-2">
            <FiUser size={25}/>
          </label>
          <label class="block text-gray-500 text-md font-bold mb-2" for="total_bayar">
            Orang Ke-{idx+1}
          </label>
        </div>
        <div className='flex flex-row w-2/3'>
          <div className='flex flex-col justify-end w-1/2'>
            <label class="block text-gray-400 text-sm" for="total_bayar">
              Harga Asli: &nbsp;
            </label>
            {patunganBedaHarga ? <label class="block text-red-400 text-sm font-bold mb-2 mr-1 line-through" for="total_bayar">
              {formatNumber(Math.round(nominalPatunganPerOrang[idx]))}
            </label>:""}

            <label class="block text-gray-400 text-sm" for="total_bayar">
              Harga Diskon: &nbsp;
            </label>
            <label class="block text-gray-400 text-sm font-bold mb-2" for="total_bayar">
              {formatNumber(Math.round(val))}
            </label>

            <label class="block text-gray-400 text-sm" for="total_bayar">
              Ongkir: &nbsp;
            </label>
            <label class="block text-gray-400 text-sm font-bold mb-2" for="total_bayar">
            {formatNumber(Math.round(hasilOngkir[idx]))}
            </label>
            
          </div>
        
          <div className='flex justify-end w-1/2'>
            <label class="block text-gray-700 text-md font-bold mb-2" for="total_bayar">
              Rp{formatNumber(Math.round(val + hasilOngkir[idx]))}
            </label>
          </div>
        </div>
      </div>)}
      
     
      <div class="flex items-center justify-between">
        <button 
        onClick={() => reset()}
        class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full"
         type="button">
          Hitung lagi
        </button>
      </div>
    </form>
  </div>

  
}

  const DaftarOrang = () =>{
   return <div>
     {
      nominalPatunganPerOrang.map((val,idx) => <div class="md:flex md:items-center mb-6">
      <div class="md:w-1/3">
        <label class="block text-gray-500 text-sm font-bold md:text-left mb-1 md:mb-0 pr-4" for="inline-full-name">
          Orang Ke-{idx+1}
        </label>
      </div>
      <div class="md:w-2/3">
        <input 
        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
        id="inline-full-name" 
        type="text"
        onChange={(event) => {
          let tmp = nominalPatunganPerOrang;
          nominalPatunganPerOrang[idx] = event.target.value;

          setNominalPatunganperOrang(tmp)
        }}
        placeholder='20.000' />
      </div>
    </div>)

     }
   </div>
  }

  return (
    <div className='bg-white shadow-md rounded lg:w-1/3 m-auto pt-10'>

      { statusHasilHitung ? <HasilHitung/> : <div class="w-full">
          <form class="px-8 pt-6 pb-8 mb-4 mt-0">
            <div class="mb-4">
              <label class="block text-gray-700 text-sm font-bold mb-2" for="total_bayar">
                Total Harga Pesanan
              </label>
              <input 
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              id="total_bayar" 
              type="text" 
              placeholder="100000" 
              value={totlaBayar} 
              onChange={(event) => setTotalbayar(event.target.value)}/>
            </div>

            <div class="mb-4">
              <label class="block text-gray-700 text-sm font-bold mb-2" for="total_discount">
                Total Ongkir ( sudah termasuk potongan ongkir / abaikan jika gratis ongkir )
              </label>
              <input 
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              id="total_discount" 
              type="text" 
              placeholder="10000"
              value={ongkir} 
              onChange={(event) => setOngkir(event.target.value)}
              />
            </div>

            <div class="mb-4">
              <label class="block text-gray-700 text-sm font-bold mb-2" for="total_discount">
                Total Discount / Potongan Harga
              </label>
              <input 
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              id="total_discount" 
              type="text" 
              placeholder="2000"
              value={potonganHarga} 
              onChange={(event) => setPotonganHarga(event.target.value)}
              />
            </div>

            <div class="mb-4">
              <label class="block text-gray-700 text-sm font-bold mb-2" for="jumlah_orang">
                Jumlah Orang
              </label>
              <input 
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
              id="jumlah_orang" 
              type="text" 
              placeholder="5"
              value={jumlahOrang} 
              onChange={(event) => {
                let tmpArry = []
                for (let i = 0; i < event.target.value; i++) {
                  tmpArry.push([])              
                }

                setNominalPatunganperOrang(tmpArry)
                setJumlahOrang(event.target.value)
              }}
              />
            </div>

            <div className='mb-4 justify-start'>
              <div class="mb-6">
                <div class="md:w-1/2"></div>
                <label class="md:w-2/3 inline-flex items-center text-gray-500 font-bold">
                  <input 
                  class="mr-2 leading-tight w-7 h-7" 
                  type="checkbox" 
                  value={patunganBedaHarga} 
                  onChange={(event) => setPatunganBedaHarga(event.target.checked)}/>
                  <span class="text-sm">
                    Centang jika anda patungan dengan item beda harga tiap orangnya
                  </span>
                </label>
              </div>
            </div>

            <hr></hr>

            <label class="block text-gray-700 text-sm font-bold mb-4 mt-2">
                Masukan Harga item per orang
            </label>

            {
              patunganBedaHarga ? <DaftarOrang/> : ""
            }
            
          
          
            <div class="flex items-center justify-between">
              <button 
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full" 
              onClick={() => hitung()}
              type="button">
                Hitung
              </button>
            </div>
          </form>
        </div>
      }
      
    </div>
  )
}